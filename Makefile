#!/usr/bin/make -f

draft = expect-signed-mail
OUTPUT = $(draft).txt $(draft).html $(draft).xml $(draft).pdf

all: $(OUTPUT)

%.xml: %.md
	kramdown-rfc2629 --v3 < $< > $@.tmp
	mv $@.tmp $@

%.html: %.xml
	xml2rfc --v3 --html $< --out $@

%.txt: %.xml
	xml2rfc --v3 --text $< --out $@

%.pdf: %.xml
	xml2rfc --v3 --pdf $< --out $@

clean:
	-rm -rf $(OUTPUT) .refcache/

check:
	codespell $(draft).md
	! grep -hn '[[:space:]]$$' $(draft).md

.PHONY: clean all check
