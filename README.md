# Expect Signed Mail

This repository attempts to define a standard for a user to signal that their e-mail messages will always be end-to-end cryptographically signed.

It will be published in [the IETF datatracker](https://datatracker.ietf.org/doc/draft-dkg-lamps-expect-signed-mail/).
